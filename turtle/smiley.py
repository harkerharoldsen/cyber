import turtle
import random
def smiley():
	try:
		turtle.TurtleScreen._RUNNING = True
		screen = turtle.Screen()
		t = turtle.Turtle()
	
		#t.tracer(0, 0)
		a = random.randint(1,100)
		b = 0
		while(b != a):
			R = random.random()
			B = random.random()
			G = random.random()

			t.color(R, G, B)
			t.speed(0)
			h = 90
			f = random.randint(-180,180)
			b = random.randint(1,15)
			g = random.randint(10,100)
			t.width(b)
			if (f<0 ):
				h = -90
			t.penup()
			t.lt(-90)
			c = random.randint(-400,300)
			d = random.randint(-400,300)
			t.goto(c,d)
			t.pendown()
			t.setheading(0)
			t.pendown()
			t.fd(1)
			t.lt(-90)
			t.penup()
			t.fd(g)
			t.pendown()
			t.lt(45)
	
			t.circle(h,f)
			t.lt(35)
			t.penup()
			t.fd(g)
			t.pendown()
			t.fd(1)
			t.penup()
			t.lt(-90)
	finally:
		turtle.Terminator()

if __name__ == '__main__':
	smiley()


